package supermarket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

public class mainpage extends Container {
    public JPanel mainpage;
    private JTabbedPane tabbedPane1;
    private JTextField ctext;
    private JTextField ntext;
    private JTextField textField1;
    private JTextField atextTextField;
    private JTextField ttext;
    private JButton جستجوButton;
    private JButton ویرایشButton;
    private JButton افزودنButton;
    private JTextField NameKalatxt;
    private JTextField Pricetxt;
    private JTextField Codekalatxt;
    private JButton اضافهکردنکالاButton;
    private JButton جستجوکالاButton;
    private JButton ویرایشکالاButton;
    private JButton حذفکالاButton;
    private JTextField NumkalaTxt;
    private JButton حذفمشتریButton;
    private JTextField textField3;
    private JTextField textField2;
    private JButton دریافتگزارشButton;
    private JList list1;
    private JList list2;
    private JButton خریدButton;
    private JButton button1;
    private JButton اضافهکردنButton;
    private JLabel label1;
    public DefaultListModel dlm2=new DefaultListModel();
    public mainpage() {
        //Customer Part Start****************************
        جستجوButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement stmt = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);


                    String sql1 = "SELECT TOP 1000 [id]\n" +
                            "      ,[codemeli]\n" +
                            "      ,[name]\n" +
                            "      ,[address]\n" +
                            "      ,[tel]\n" +
                            "  FROM [supermarket].[dbo].[Userinfo] where id=?";

                    PreparedStatement ss = con.prepareStatement(sql1);
                    ss.setString(1, textField1.getText());
                    rs = ss.executeQuery();

                    while (rs.next()) {
                        System.out.println(rs.getString(1) + " " + rs.getString(2));
                        String s = rs.getString(2);
                        String s1 = rs.getString(3);
                        String s3 = rs.getString(4);
                        String s4 = rs.getString(5);
                        ctext.setText(s);
                        ntext.setText(s1);
                        atextTextField.setText(s3);
                        ttext.setText(s4);


                    }

                } catch (Exception g) {

                }
            }
        });
        افزودنButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement stmt = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);

                    String sql2 = "exec addUser ?,?,?,?,?";
                    PreparedStatement ss = con.prepareStatement(sql2);
                    ss.setString(1, textField1.getText());
                    ss.setString(2, ctext.getText());
                    ss.setString(3, ntext.getText());
                    ss.setString(4, atextTextField.getText());
                    ss.setString(5, ttext.getText());
                    rs = ss.executeQuery();
                    while (rs.next()) {
                        System.out.println(rs.getString(1) + " " + rs.getString(2));
                    }

                } catch (Exception g) {

                }
            }
        });
        ویرایشButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement ss = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);

                    String sql3 = "update Userinfo set codemeli=" + ctext.getText() + ",name=" + ntext.getText() + ",address=" + atextTextField.getText() + ",tel=" + ttext.getText() + " where id=" + textField1.getText();
                    ss = con.createStatement();
                    ss.executeUpdate(sql3);

                } catch (Exception g) {

                }
            }
        });
        حذفمشتریButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                //  Statement ss= null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);

                    String sql3 = "delete from Userinfo where id=?";
                    PreparedStatement ss = con.prepareStatement(sql3);
                    ss.setString(1, textField1.getText());
                    ss.executeUpdate(sql3);

                } catch (Exception g) {

                }

            }
        });
        //End Customer*************************************************************

        اضافهکردنکالاButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement stmt = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);

                    String sql2 = "exec addkala ?,?,?,?";
                    PreparedStatement ss = con.prepareStatement(sql2);
                    ss.setString(1, Codekalatxt.getText());
                    ss.setString(2, NameKalatxt.getText());
                    ss.setString(3, Pricetxt.getText());
                    ss.setString(4, NumkalaTxt.getText());

                    rs = ss.executeQuery();
                    while (rs.next()) {
                        System.out.println(rs.getString(1) + " " + rs.getString(2));
                    }

                } catch (Exception g) {
                    //JOptionPane.showMessageDialog(this, e.getMessage());
                }

            }
        });

        جستجوکالاButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement stmt = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);


                    String sql1 = "SELECT TOP 1000 [CodeKala]\n" +
                            "      ,[NameKala]\n" +
                            "      ,[Price]\n" +
                            "      ,[num]\n" +
                            "  FROM [supermarket].[dbo].[kala] where [CodeKala]=?";

                    PreparedStatement ss = con.prepareStatement(sql1);
                    ss.setString(1, Codekalatxt.getText());
                    rs = ss.executeQuery();

                    while (rs.next()) {
                        System.out.println(rs.getString(1) + " " + rs.getString(2));
                        String s = rs.getString(2);
                        String s1 = rs.getString(3);
                        String s3 = rs.getString(4);

                        NameKalatxt.setText(s);
                        Pricetxt.setText(s1);
                        NumkalaTxt.setText(s3);


                    }

                } catch (Exception g) {
                    //JOptionPane.showMessageDialog(this, e.getMessage());
                }

            }
        });
        ویرایشکالاButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement ss = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);

                    String sql3 = "update kala set CodeKala=" + Codekalatxt.getText() + ",NameKala=" + NameKalatxt.getText() + ",Price=" + Pricetxt.getText() + ",num=" + NumkalaTxt.getText();
                    ss = con.createStatement();
                    ss.executeUpdate(sql3);

                } catch (Exception g) {
                    //JOptionPane.showMessageDialog(this, e.getMessage());
                }

            }
        });
        حذفکالاButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                //  Statement ss= null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);

                    String sql3 = "delete FROM kala WHERE CodeKala=?";
                    PreparedStatement ss = con.prepareStatement(sql3);
                    ss.setString(1, textField1.getText());
                    ss.executeUpdate(sql3);

                } catch (Exception g) {
                    //JOptionPane.showMessageDialog(this, e.getMessage());
                }

            }
        });
        دریافتگزارشButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                        "databaseName=supermarket;user=sa;password=5436590";

                // Declare the JDBC objects.
                Connection con = null;
                Statement stmt = null;
                ResultSet rs = null;

                try {
                    // Establish the connection.
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    con = DriverManager.getConnection(connectionUrl);


                    String sql1 = "SELECT * FROM kala where CodeKala=?";

                    PreparedStatement ss = con.prepareStatement(sql1);
                    ss.setString(1, textField3.getText());
                    rs = ss.executeQuery();

                    while (rs.next()) {


                        String s3 = rs.getString(4);


                        textField2.setText(s3);


                    }

                } catch (Exception g) {
                    JOptionPane.showMessageDialog(null, "erore");
                }
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadlist();
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loadlist();
            }
        });


        خریدButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {JOptionPane.showMessageDialog(null,"خرید انجام شد!");
                for (int i = dlm2.getSize() - 1; i >= 0; i--) {
                    dlm2.removeElement(dlm2.elementAt(i));
                }
                list2.setModel(dlm2);
            }

        });
        list1.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (list1.getSelectedIndices().length > 0) {
                    int[] selectedIndices = list1.getSelectedIndices();
                    //append selected to list2
                    for (int i = 0; i < selectedIndices.length; i++) {
                        dlm2.addElement(list1.getModel().getElementAt(selectedIndices[i]));
                    }}
                  list2.setModel(dlm2);
                super.mousePressed(e);


                }
        });

    }

    public void loadlist(){
        String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
                "databaseName=supermarket;user=sa;password=5436590";

        // Declare the JDBC objects.
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // Establish the connection.
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);


            String sql1="SELECT * FROM kala ";

            PreparedStatement ss=con.prepareStatement(sql1);

            rs = ss.executeQuery();
            DefaultListModel dlm=new DefaultListModel();
            while(rs.next()){
                dlm.addElement(rs.getString(2));
            }
list1.setModel(dlm);
        }
            catch (Exception g){

            }

    }


    public static void mainpage(){
        JFrame frame2=new JFrame("general page");
        frame2.setContentPane(new mainpage().mainpage);
        frame2.pack();
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setVisible(true);

        
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
